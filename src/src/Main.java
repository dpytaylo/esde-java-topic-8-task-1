package src;

public class Main {
    public static void main(String[] args) {
        var task = new Task1(2, 6);

        task.display();

        task.setA(4);
        task.setB(2);

        System.out.println("sum = " + task.sum());
        System.out.println("max = " + task.max());
    }
}